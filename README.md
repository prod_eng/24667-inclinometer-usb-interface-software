24667 Inclinometer USB Interface Software
This is the K20 firmware implementing USB CDC and UART input from MEAS-series inclinometers.
5 bits of GPIO are read with each angle packet.  The 5 bits provide a way to set an individual ID for 
each connected inclinometer.  If all 5 bits are zero, the angle packet is passed un-altered.  For non-
zero (1 - 31) values, the packet is prefixed with the string, "N=xx\r\n", where the 'xx' is the binary
value of the 5 bits.
