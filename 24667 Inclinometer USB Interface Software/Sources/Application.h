/**---------------------------------------------------------------------------------------------------------------------
 * @file            Application.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Dec 5, 2018
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_APPLICATION_H_
#define SOURCES_APPLICATION_H_


#define FRDM_BOARD // Use the little red freescale board until our PCBs arrive

#include "CPU.h"
#include "Events.h"
#include "FRTOS1.h"
#include "CLS1.h"
#include "MCUC1.h"
#include "USB0.h"
#include "USB1.h"
#include "CDC1.h"
#include "Tx1.h"
#include "Rx1.h"
#include "UTIL1.h"
#include "XF1.h"
#include "CS1.h"
#include "TMOUT1.h"
#include "WAIT1.h"
#include "KIN1.h"
#include "INCL_IO.h"
#include "ASerialLdd1.h"
#include "ID_BIT4.h"
#include "BitIoLdd1.h"
#include "ID_BITS_0TO3.h"
#include "BitsIoLdd1.h"
#include "CI2C1.h"
#include "IntI2cLdd1.h"
#include "HF1.h"
#include "WDG1.h"
#include "WatchDogLdd1.h"
#include "IO_C6.h"
#include "BitIoLdd2.h"
#include "IO_C7.h"
#include "BitIoLdd3.h"
#include "IO_D4.h"
#include "BitIoLdd4.h"
#include "IO_D5.h"
#include "BitIoLdd5.h"
#include "PTC.h"
/* Including shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"


#include "stdio.h"
#include "string.h"
#include "USBTaskModule.h"
#include "InclinometerSerialModule.h"


void vApplicationInitTasks(void);

#endif // SOURCES_APPLICATION_H_
