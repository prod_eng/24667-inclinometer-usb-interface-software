/**---------------------------------------------------------------------------------------------------------------------
 * @file            InclinometerSerialModule.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Mar 31, 2018
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_INCLINOMETERSERIALMODULE_H_
#define SOURCES_INCLINOMETERSERIALMODULE_H_



void vInclinometerCommTaskInit( uint8 u8Priority, uint16 u16StackSize );


void vInclinometerTransmitPacket( uint8 *pBufferToSend, uint16 u16Length );


void vISR_InclinometerCharsAvailable(void);

#endif // SOURCES_INCLINOMETERSERIALMODULE_H_
