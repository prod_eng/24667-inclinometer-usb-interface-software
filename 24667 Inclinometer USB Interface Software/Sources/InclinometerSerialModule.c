/**---------------------------------------------------------------------------------------------------------------------
 * @file            InclinometerSerialModule.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Mar 31, 2018
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "Application.h"

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
typedef enum ePingPongBufferType
{
    eBUFFER_A = 0,
    eBUFFER_B,
    //
    eBUFFER_COUNT
}ePING_PONG_BUFFER_TYPE;

#define cINCLINOMETER_RX_BUFFER_SIZE      ( 22 )



/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */
// Serial "ping-pong" receive buffers
static uint8 gu8InclinometerRxScratchBuffer[eBUFFER_COUNT][cINCLINOMETER_RX_BUFFER_SIZE + 1]; // Enough room for 2 X-Y packets plus null terminators

static uint8 gu8Prefix[7] = "N=00\r\n\0";
static const uint16 cTENS_BYTE_POS = 2;
static const uint16 cONES_BYTE_POS = 3;
static uint16 gu16RxIndex = 0;
static const uint8 cPACKET_START_CHAR = 'X';
static ePING_PONG_BUFFER_TYPE geActiveBuffer = eBUFFER_A;


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vProcessActiveBuffer( ePING_PONG_BUFFER_TYPE eBuf );



/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vInclinometerTransmitPacket( uint8 *pBufferToSend, uint16 u16Length )
{
    return;
}




/**---------------------------------------------------------------------------------------------------------------------
 * @func        vISR_InclinometerCharsAvailable() The Rx Char ISR called from Events.c when the Rx interrupt fires
 *
 * @brief       Since Rx'd chars arrive so slowly, and packets are fixed size, we are being inefficient and doing the full
 *               packet decode and forward inside the ISR when the ping-pong buffer is full.  If bandwidth or consistent
 *               interrupt timing becomes a concern, this should be modified to push characters into a Queue for back-
 *               ground processing.
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vISR_InclinometerCharsAvailable(void)
{
    uint16 u16Count = 1;
    uint8 u8Char;
    uint8 u8Result;

    while( u16Count )   // at least one char is here, or we wouldn't have been interrupted
    {
        u8Result = INCL_IO_RecvChar( &u8Char );

        if( ERR_OK == u8Result ) // if we have a good Rx char
        {
            if( cPACKET_START_CHAR == u8Char )
            {
                gu16RxIndex = 0;
            }

            gu8InclinometerRxScratchBuffer[ geActiveBuffer ][ gu16RxIndex++ ] = u8Char;

            if( cINCLINOMETER_RX_BUFFER_SIZE == gu16RxIndex ) // If we've filled the buffer...
            {
                gu16RxIndex = 0; // Reset the Index

                if( eBUFFER_A == geActiveBuffer ) // And switch to the opposite buffer
                {
                    vProcessActiveBuffer( eBUFFER_A );
                    geActiveBuffer = eBUFFER_B;
                    IO_D4_ClrVal();

                }
                else
                {
                    vProcessActiveBuffer( eBUFFER_B );
                    geActiveBuffer = eBUFFER_A;
                    IO_D4_SetVal();
                }

            }

        }
        else
        {
            // Receive Error occurred
            INCL_IO_ClearRxBuf();
            u16Count = 0;
        }

        u16Count = INCL_IO_GetCharsInRxBuf();   // check if any more chars are waiting

    } // end of while

    return;
}




/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vProcessActiveBuffer( ePING_PONG_BUFFER_TYPE eBuf )
{
    uint8 u8ShelfSwitch = ( ID_BITS_0TO3_GetVal() );    // Get the lower 4-bits of the shelf ID from the rotary switch
    u8ShelfSwitch |= ( ID_BIT4_GetVal() << 4 );         // Get the upper bit of the shelf ID from the R8 input
    gu8Prefix[cTENS_BYTE_POS] = '0';    // Load our defaults
    gu8Prefix[cONES_BYTE_POS] = '1';

    if( u8ShelfSwitch && ( u8ShelfSwitch < 32 ) ) // Look for valid non-zero shelf value (only 5 significant bits)
    {
        if( u8ShelfSwitch >= 30 )
        {
            gu8Prefix[cTENS_BYTE_POS] = '3';
            gu8Prefix[cONES_BYTE_POS] = ( ( u8ShelfSwitch - 30 ) + '0' );
        }
        else if( u8ShelfSwitch >= 20 )
        {
            gu8Prefix[cTENS_BYTE_POS] = '2';
            gu8Prefix[cONES_BYTE_POS] = ( ( u8ShelfSwitch - 20 ) + '0' );
        }
        else if( u8ShelfSwitch >= 10 )
        {
            gu8Prefix[cTENS_BYTE_POS] = '1';
            gu8Prefix[cONES_BYTE_POS] = ( ( u8ShelfSwitch - 10 ) + '0' );
        }
        else
        {
            gu8Prefix[cONES_BYTE_POS] = ( u8ShelfSwitch + '0' );
        }

        CLS1_SendStr( gu8Prefix, CLS1_SendChar );
    } // else don't prefix (should never have more than 5-bits of address.)

    // Now send the inclinometer packet
    CLS1_SendStr( &gu8InclinometerRxScratchBuffer[eBuf][0], CLS1_SendChar );

    return;
}




