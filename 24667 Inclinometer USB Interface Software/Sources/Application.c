/**---------------------------------------------------------------------------------------------------------------------
 * @file            Application.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Dec 5, 2018
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "Application.h"

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vApplicationInitTasks(void)
{
    vAppUSBTaskInit();  // Kind of dumb that we have only this one Task module, but here's room for future growth...
                        // Certainly the compiler will inline this one.

    return;
}


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */



