/**---------------------------------------------------------------------------------------------------------------------
 * @file            USBTaskModule.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Jan 12, 2019
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_USBTASKMODULE_H_
#define SOURCES_USBTASKMODULE_H_

void vAppUSBTaskInit(void);


#endif // SOURCES_USBTASKMODULE_H_
