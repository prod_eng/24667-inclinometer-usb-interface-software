/**---------------------------------------------------------------------------------------------------------------------
 * @file            USBTaskModule.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Jan 12, 2019
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#include "Application.h"

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */
static TaskHandle_t xMyTaskHandle               = NULL;

static uint8_t cdc_buffer[USB1_DATA_BUFF_SIZE];


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vUSBCDCTask( void *pvParameters );


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vAppUSBTaskInit(void)
{
    if( TRUE != FRTOS1_xTaskCreate(
        vUSBCDCTask,
        "CDC_Task",
        96,
        (void *)NULL,
        3,
        xMyTaskHandle )
    )
    {
        // Failed to create task
        for(;;);
    }

    return;
}




/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vUSBCDCTask( void *pvParameters )
{
    uint8 u8count = 0;
    bool boLastState = FALSE;

    for(;;)
    {
        while( ERR_BUSOFF == CDC1_App_Task( cdc_buffer, sizeof(cdc_buffer) ) )
        {
            /* device not enumerated */
            WDG1_Clear();   // Kick the dog
        }

        // Blink the green LED slowly when no inclinometer packets are available.
        if( 30 == u8count++ )
        {
            u8count = 0;
            if( boLastState )
            {
                IO_D4_SetVal();
                boLastState = FALSE;
            }
            else
            {
                IO_D4_ClrVal();
                boLastState = TRUE;
            }

        }

        vTaskDelay( 10 );
        WDG1_Clear();   // Kick the dog
    }
    // Task functions do not return.
}


